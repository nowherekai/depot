require 'test_helper'

class LineItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ruby = products(:ruby)
  end

  test "should add line_item to cart" do
    assert_difference "LineItem.count" do
      post line_items_path, params: { product_id: @ruby.id }
    end
    assert_redirected_to store_path
    follow_redirect!

    assert_select "#cart h2", "Your Pragmatic Cart"
    assert_select "#cart td", /Programming Ruby 1.9/
  end

  test "should add line_item to cart via ajx" do
    assert_difference "LineItem.count" do
      post line_items_path, params: { product_id: @ruby.id }, xhr: true
    end

    assert_response :success
    # assert_select_jquery :html, '#cart' do
      # assert_select 'tr#current_item td', /Programming Ruby 1.9/
    # end
  end

  test 'create line_item of same product' do
    assert_difference "LineItem.count" do
      post line_items_path, params: { product_id: @ruby.id }
    end

    assert_no_difference "LineItem.count" do
      post line_items_path, params: { product_id: @ruby.id }
    end

    cart = Cart.find session[:cart_id]
    item = cart.line_items.find_by(product_id: @ruby.id)
    assert_equal 2, item.quantity
  end
end
