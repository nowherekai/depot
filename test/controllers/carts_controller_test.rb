require 'test_helper'

class CartsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get cart_path
    assert_response :success
  end

  test 'should empty cart' do
    post line_items_path, params: { product_id: products(:ruby).id }
    cart = Cart.find session[:cart_id]
    assert_equal 1, cart.line_items.size
    delete empty_cart_path
    assert_equal 0, cart.line_items.reload.size
  end
end
