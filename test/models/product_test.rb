require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test 'product attributes must not be empty' do
    product = Product.new
    product.valid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:image_url].any?
    assert product.errors[:price].any?
  end

  test 'product price must be positive' do
    product = Product.new(title: "test title",
                          description: "etst",
                          image_url: "zzz.jpg")
    product.price = -1
    product.valid?
    assert_equal ["must be greater than or equal to 0.01"], product.errors[:price]

    product.price = 0
    product.valid?
    assert_equal ["must be greater than or equal to 0.01"], product.errors[:price]

    product.price = 0.01
    assert product.valid?

    product.price = 13.45
    assert product.valid?
  end

  test 'can not destroy product if line items present' do
    ruby = products(:ruby)
    assert ruby.line_items.present?
    ruby.destroy
    assert ruby.persisted?
  end
end
