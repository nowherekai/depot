require 'test_helper'

class LineItemTest < ActiveSupport::TestCase
  setup do
    @ruby = products(:ruby)
    @cart = carts(:one)
  end

  test "line_item product must exist" do
    line_item = LineItem.new
    line_item.valid?
    assert line_item.errors[:product].present?
  end

  test 'line_item cart must exist when create' do
    line_item = LineItem.create(product: @ruby)
    assert line_item.errors[:cart].present?
    line_item.cart = @cart
    assert line_item.valid?

    line_item.save!
    line_item.cart_id = nil
    assert line_item.valid?
  end
end
