require 'test_helper'

class OrderNotifierMailerTest < ActionMailer::TestCase
  setup do
    @order = orders(:one)
  end

  test "received" do
    mail = OrderNotifierMailer.received(@order.id)
    assert_equal "Your Order Confirmation", mail.subject
    assert_equal [@order.email], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match /1 x Programming Ruby 1.9/, mail.body.encoded
  end

  test "shipped" do
    mail = OrderNotifierMailer.shipped(@order.id)
    assert_equal "Your Order Shipped", mail.subject
    assert_equal [@order.email], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match /<td>1&times;<\/td>\s*<td>Programming Ruby 1.9<\/td>/, mail.body.encoded
  end

end
