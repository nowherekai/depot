Rails.application.routes.draw do
  get 'orders/new'

  resources :products
  resource :cart, only: [:show] do
    delete :empty, on: :member
  end
  resources :line_items, only: [:create]
  resources :orders
  root 'store#index', as: :store
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  mount ActionCable.server => '/cable'
end
