class Product < ApplicationRecord
  has_many :line_items

  before_destroy :ensure_not_referenced_by_any_line_item

  validates :title, presence: true, uniqueness: true
  validates :description, presence: true
  validates :image_url, presence: true, format: { with: %r{\.(gif|jpg|png)\Z}i, message: "must be a URL for GIF, JPG or PNG image.", allow_blank: true }
  validates :price, numericality: { greater_than_or_equal_to: 0.01 }

  private

  def ensure_not_referenced_by_any_line_item
    unless line_items.empty?
      errors.add(:base, "Line Items presnet")
      throw :abort
    end
  end
end
