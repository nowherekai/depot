class LineItem < ApplicationRecord
  belongs_to :product
  belongs_to :cart, optional: true
  belongs_to :order, optional: true

  before_validation :set_price, on: :create

  validate :cart_required, on: :create
  validates :price, numericality: { greater_than_or_equal_to: 0.01 }
  validates :quantity, presence: true, numericality: { greater_than_or_equal_to: 1, only_integer: true  }

  def total_price
    price * quantity
  end

  private

  def cart_required
    errors.add(:cart, "cart must existed on create!") unless cart.present?
  end

  def set_price
    self.price = product&.price
  end
end
