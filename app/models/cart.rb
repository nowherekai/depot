class Cart < ApplicationRecord
  has_many :line_items

  def empty!
    line_items.destroy_all
  end

  def add_to_cart(product_id)
    current_item = line_items.where(product_id: product_id).first
    if current_item
      current_item.quantity += 1
    else
      current_item = line_items.build(product_id: product_id)
    end
    current_item
  end

  def total_price
    line_items.sum(&:total_price)
  end
end
