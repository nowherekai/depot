class CartsController < ApplicationController
  def show
  end

  def empty
    current_cart.empty!
    redirect_to store_path, notice: "Your cart has been empty!"
  end
end
