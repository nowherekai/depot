class LineItemsController < ApplicationController
  def create
    @line_item = current_cart.add_to_cart(params[:product_id])

    @line_item.save

    respond_to do |format|
      format.html { redirect_to store_url }
      format.js { @current_item = @line_item }
    end
  end
end
