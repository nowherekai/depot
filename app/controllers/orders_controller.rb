class OrdersController < ApplicationController
  def new
    if current_cart.line_items.empty?
      redirect_to store_url, notice: "Your cart is empty"
      return
    end

    @order = Order.new
  end

  def create
    @order = Order.new(params.require(:order).permit(:name, :address, :email, :pay_type))
    @order.add_line_items_from_cart(current_cart)

    if @order.save
      redirect_to store_path
    else
      render :new
    end
  end
end
