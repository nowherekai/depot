class OrderNotifierMailer < ApplicationMailer
  def received(order_id)
    @order = Order.find order_id
    mail to: @order.email, subject: "Your Order Confirmation"
  end

  def shipped(order_id)
    @order = Order.find order_id
    mail to: @order.email, subject: "Your Order Shipped"
  end
end
