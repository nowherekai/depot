class AddPriceToLineItem < ActiveRecord::Migration[5.0]
  def change
    add_column :line_items, :price, :decimal, precision: 8, scale: 2

    reversible do |dir|
      LineItem.reset_column_information
      dir.up do
        LineItem.includes(:product).find_each do |line_item|
          line_item.price = line_item.product.price
          line_item.save!
        end
        # ActiveRecord::Base.connection.execute <<~SQL
        # SQL
      end

      dir.down { }
    end
  end
end
