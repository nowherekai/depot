class AddQuantityToLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :line_items, :quantity, :integer, default: 1

    reversible do |dir|
      LineItem.reset_column_information

      dir.up do
        Cart.all.each do |cart|
          sums = cart.line_items.group(:product_id).sum(:quantity)

          sums.each do |product_id, quantity|
            if quantity > 1
              cart.line_items.where(product_id: product_id).delete_all

              cart.line_items.create!(product_id: product_id, quantity: quantity)
            end
          end
        end
      end

      dir.down {}
    end
  end
end
